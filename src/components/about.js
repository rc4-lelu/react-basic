import React, {Component} from 'react';

class About extends Component {
    constructor(props) {
        super(props);
        this.state = {
            skillValue: '',
            title: "Un titre",
            contact: {name: 'rc4-lelu', profile: "images/logo512.png", email: 'lelu@me.com'},
            skills: [
                {id: 1, skill: "Programmer"},
                {id: 2, skill: "Machine learning"},
                {id: 3, skill: "UI design"}
            ]
        }
    }

    setSkill = (event) => {
        this.setState({
            skillValue: event.target.value
        })
    }
    addSkill = (event) => {
        event.preventDefault();
        let skill = {
            id: [...this.state.skills].pop().id + 1,
            skill: this.state.skillValue
        }
        this.setState({
            skills: [...this.state.skills, skill]
        })
    }
    onDelete = (skill) => {
        let index = this.state.skills.indexOf(skill)
        let listSkills = [...this.state.skills];
        listSkills.splice(index, 1)
        this.setState({
            skills: listSkills
        })
    }

    render() {
        return (
            <div className="m-4">
                <div className="card">
                    <div className="card-header">
                        <strong className="m-2">{this.state.title}</strong>
                    </div>
                    <div className="row p-2">
                        <div className="col col-auto">
                            <img width={50} src={this.state.contact.profile}/>
                        </div>
                        <div className="col">
                            <ul className="list-group">
                                <li className="list-group-item">{this.state.contact.name}</li>
                                <li className="list-group-item">{this.state.contact.email}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="card mt-3">
                    <div className="card-header">Skills</div>
                    <div className="card-body">
                        <form onSubmit={this.addSkill}>
                            <input type="text"
                                   name="skill"
                                   value={this.state.skillValue}
                                   onChange={this.setSkill}
                                   placeholder='New skill'/>
                            <button className="btn btn-primary m-2" type="submit">Add</button>
                        </form>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Skills</th>
                                </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.skills.map((skill, index) =>
                                    <tr key={index}>
                                        <td>{skill.id}</td>
                                        <td>{skill.skill}</td>
                                        <td>
                                            <button className="btn btn-danger btn-sm"
                                                    onClick={() => this.onDelete(skill)}>Supprimer
                                            </button>
                                        </td>
                                    </tr>
                                )
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default About;