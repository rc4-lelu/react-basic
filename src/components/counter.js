import React, {Component} from 'react';

class Counter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            counter: 1,
            list: [0]
        }
    }

    compute=(op) => {
        let signe = op === '+' ? 1 : -1;
        if (this.state.counter == 1 && op === '-') signe = 0;
        let counter = this.state.counter + signe;
        this.setState(
            {
                counter: counter,
                list: new Array(counter).fill(0)
            }
        );
    }

    render() {
        return (
            <div className="m-3">
                <div className="card">
                    <div className="card-header">
                        <strong>{this.props.title ? this.props.title : 'Default title'} : {this.state.counter}</strong>
                    </div>
                    <div className="ms-auto">
                        <button onClick={() => this.compute('+')} className="btn btn-primary m-2">+</button>
                        <button onClick={() => this.compute('-')} className="btn btn-primary m-2">-</button>
                    </div>
                    <div className="card-body">
                        {
                            this.state.list.map((val, index) =>
                                <img key={index} width={100} src={this.props.image ? this.props.image : 'images/logo512.png'}/>
                            )
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default Counter;