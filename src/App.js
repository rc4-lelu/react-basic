import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import Counter from "./components/counter";
import {Route, Switch, Link, BrowserRouter as Router, Routes} from "react-router-dom";
import Home from "./components/home";
import About from "./components/about";
import Gallery from "./components/gallery";

function App() {
  return (
    <Router>
        <nav className="navbar navbar-expand navbar-brand m-3">
            <ul className="navbar-nav">
                <li>
                    <Link className="nav-link" to="/home">Home</Link>
                </li>
                <li>
                    <Link className="nav-link" to="/counter">Counter</Link>
                </li>
                <li>
                    <Link className="nav-link" to="/about">About</Link>
                </li>
                <li>
                    <Link className="nav-link" to="/gallery">Gallery</Link>
                </li>
            </ul>
        </nav>
        <Routes>
            <Route exact path="/counter" element={<Counter/>}> </Route>
            <Route exact path="/home" element={<Home/>}> </Route>
            <Route exact path="/about" element={<About/>}> </Route>
            <Route exact path="/gallery" element={<Gallery/>}> </Route>
        </Routes>
    </Router>
  );
}

export default App;
